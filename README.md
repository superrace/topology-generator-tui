# README #
Name: Topology Generator Taxt-based User Interface
(for Mininet and ACRF)

Auth : Mengchen SHI

Email : mcshi.tommy@gmail.com

Version : 7.0 - 5 Apr. 2017
## Whats News
* The mininet script generated can save the nodes and links information to temp files
* There is an application 'sendRPCbyfile.py' which call RPCagent to send PRC message to RPCserver(VMServer). 
* Use 'sh python sendRPCbyfile.py' in Mininet to send RPC messages to ACRF-extended platform.
* Only for RouteFlow use

-------------------------
## Requirement 

* Mininet  
* OVS
* python-networkx  
* python-numpy  
* python-matplotlib  
* python-pydot

sudo apt-get install python-numpy python-pydot python-matplotlib python-networkx
Those requirements can also be installed by pip
####NetworkX version 1.6 are highly recommanded.
pip install numpy pydot matplotlib networkx==1.6

## What Network can be generated 

* Fullmesh  
* Ring  
* Star  
* Linear  
* Binary Tree  
* 2-D Mesh  
* Topology ZOO  
* Random Network 
  * Erdos renyi graph  
      * n (int) – The number of nodes.  
      * p (float) – Probability for edge creation).  
  * Random graph  
      * n (int) – The number of nodes.  
      * m (int) – The number of edges.  
  * More: see https://networkx.github.io/documentation/networkx-1.10/reference/generators.html  
  * Introduce:https://www.youtube.com/watch?v=yisN77RMAAA  
* Custom by miniedit  
(Until V6.0)

## What can this repository do?###
* Generate Mininet topology
* Plot the network topology 
* Customize the name of switches as Label on plot
* Customize the protocol for Automatic Config Routeflow(ACRF) CONFIG FILE
* OSPF or BGP + OSPF
* Colored display of different AS as Legend

## How do I get set up? ###

* Install Mininet and OVS
* Clone this repo
* cd topogenerator
* sudo python tg-tui.py
* follow the guide

## Note ###
* Every switch will use first port connect with one host
* Host's IP address will be assigned auto for 172.168.[switch number].100
* Default gw is 172.168.[host number].1 
* You can use Mininet default IP by comment the line of IP address for host in MininetParser.py. 
* Using Miniedit for ACRF should follow the following steps

## How to assigne IPCONF for other Mininet Script for ACRF###
Use ip.py to generator a sequence of CLI command to assign the ip address and controller for any mininet on fly

### Sample usage: ####
		python ip.py -n 8 -c 130.149.221.202 -p 6600 -t -o ipconf  
		-n : Number of hosts  
		-c : Controller IP  
		-p : controller Port  
		-t : Print the commands generated  
		-e : For Miniedit (will add controller command line)  
		-o : Filename for output file, Default: ipconf.  

### In mininet CLI on fly ###
		source ipconf  

## How to Customize topology using miniedit and assign IP address for ACRF ###
		cd topogenerator/  
		sudo python ~/mininet/examples/miniedit.py  
> Follow the steps very clearfully  
>  1. Add switches **one by one** in miniedit  
>  2. Add hosts one by one by [THE SAME] order  
>  3. Add links between host and switches [s1 <-> h1, h2 <-> s2 .etc]  
>  4. Add any links you want between switches  
>  5. Click 'EDIT' -> "Preformance" -> 'Start CLI' -> OK  
>  6. Run the Topology  
>  7. Assign ip address and controller by run this command in Mininet CLI:  
>     sh python ip.py -n 6 -e -p -c 130.149.221.202 -p 6600  
>     *  '6' is the number of the switches you draw in miniedit  
>     *  '130.149.221.202' is the controller ip  
>     *  '6600' is the controller port  
>  8. type ' source ipconf' in mininet CLI on fly  
>  9. Start the ACRF and done  
