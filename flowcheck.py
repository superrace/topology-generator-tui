#!/usr/bin/python
import commands
import time
from sys import argv
max = 15
start = time.time()
for i in range(len(argv)):
    if argv[i] == '-m':
        max = int(argv[i+1])
print "->   Wait for initial time {} seconds.".format(max*2)
time.sleep(max*2)
print "->   Start to check reachability."
r = 0
while True:
	i = 0
	r += 1	
	for x in range(1, max+1):
		for y in range(1, max+1):
			if y !=x:
				command = "sudo ovs-ofctl dump-flows s{} | grep 172.168.{}.0".format(x,y)
				#print command
				if len(commands.getoutput(command)) != 0:
					i += 1
					#print '{} have  {}  in flowtable!'.format(x,y)
					#print len(commands.getoutput(command)) 
				else:
					if r > (max):
					# 	print "RESTART {}".format(x)
				 	# if (time.time()-start) > max*6:
						print 'S{} have no 172.168.{}.0/24 in flowtable'.format(x,y)
				# 	# only exit when all output != 0 and give a time 
	left = ((max * (max-1))-i)
	print "->   Checking flowtables................{} flows missing.".format(left)
	if left > 0: 
		time.sleep(10)
	# print str(i) + ' flows'
	if i == max * (max-1):	
		print "->   All switches should reachable now. Try 'pingall' in mininet."
		break
	if (time.time()-start) > 300:
		print "->   5 Minutes passed, stop checking flow tables. Please check reachability and debug the system."
		break
print 'done'